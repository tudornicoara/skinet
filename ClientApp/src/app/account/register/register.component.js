"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(fb, accountService, router, toastr) {
        this.fb = fb;
        this.accountService = accountService;
        this.router = router;
        this.toastr = toastr;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.createRegisterForm();
    };
    RegisterComponent.prototype.createRegisterForm = function () {
        this.registerForm = this.fb.group({
            displayName: [null, [forms_1.Validators.required]],
            email: [null, [forms_1.Validators.required,
                    forms_1.Validators.pattern('^[\\w-\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')],
                [this.validateEmailNotTaken()]],
            password: [null, [forms_1.Validators.required]]
        });
    };
    RegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.accountService.register(this.registerForm.value).subscribe(function (response) {
            _this.router.navigateByUrl('/shop');
            _this.toastr.success('Signed in');
        }, function (error) {
            console.log(error);
            _this.errors = error.errors;
        });
    };
    RegisterComponent.prototype.validateEmailNotTaken = function () {
        var _this = this;
        return function (control) {
            return rxjs_1.timer(500).pipe((operators_1.switchMap(function () {
                if (!control.value) {
                    return rxjs_1.of(null);
                }
                return _this.accountService.checkEmailExists(control.value).pipe(operators_1.map(function (res) {
                    return res ? { emailExists: true } : null;
                }));
            })));
        };
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'app-register',
            templateUrl: './register.component.html',
            styleUrls: ['./register.component.css']
        })
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map