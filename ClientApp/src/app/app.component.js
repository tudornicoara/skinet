"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppComponent = void 0;
var core_1 = require("@angular/core");
var AppComponent = /** @class */ (function () {
    function AppComponent(basketService, accountService) {
        this.basketService = basketService;
        this.accountService = accountService;
        this.title = 'Skinet';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.loadBasket();
        this.loadCurrentUser();
    };
    AppComponent.prototype.loadCurrentUser = function () {
        var token = localStorage.getItem('token');
        this.accountService.loadCurrentUser(token).subscribe(function () {
            console.log('loaded user');
        }, function (error) {
            console.log(error);
        });
    };
    AppComponent.prototype.loadBasket = function () {
        var basketId = localStorage.getItem('basket_id');
        if (basketId) {
            this.basketService.getBasket(basketId).subscribe(function () {
                console.log('initialized bakset');
            }, function (error) {
                console.log(error);
            });
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html'
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map