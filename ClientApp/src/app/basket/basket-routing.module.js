"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasketRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var basket_component_1 = require("./basket.component");
var routes = [
    { path: '', component: basket_component_1.BasketComponent }
];
var BasketRoutingModule = /** @class */ (function () {
    function BasketRoutingModule() {
    }
    BasketRoutingModule = __decorate([
        core_1.NgModule({
            declarations: [],
            imports: [
                router_1.RouterModule.forChild(routes)
            ],
            exports: [router_1.RouterModule]
        })
    ], BasketRoutingModule);
    return BasketRoutingModule;
}());
exports.BasketRoutingModule = BasketRoutingModule;
//# sourceMappingURL=basket-routing.module.js.map