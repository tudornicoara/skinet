"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasketModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var basket_component_1 = require("./basket.component");
var basket_routing_module_1 = require("./basket-routing.module");
var shared_module_1 = require("../shared/shared.module");
var BasketModule = /** @class */ (function () {
    function BasketModule() {
    }
    BasketModule = __decorate([
        core_1.NgModule({
            declarations: [basket_component_1.BasketComponent],
            imports: [
                common_1.CommonModule,
                basket_routing_module_1.BasketRoutingModule,
                shared_module_1.SharedModule
            ]
        })
    ], BasketModule);
    return BasketModule;
}());
exports.BasketModule = BasketModule;
//# sourceMappingURL=basket.module.js.map