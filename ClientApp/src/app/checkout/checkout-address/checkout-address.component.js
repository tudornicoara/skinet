"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutAddressComponent = void 0;
var core_1 = require("@angular/core");
var CheckoutAddressComponent = /** @class */ (function () {
    function CheckoutAddressComponent(accountService, toastr) {
        this.accountService = accountService;
        this.toastr = toastr;
    }
    CheckoutAddressComponent.prototype.ngOnInit = function () {
    };
    CheckoutAddressComponent.prototype.saveUserAddress = function () {
        var _this = this;
        this.accountService.updateUserAddress(this.checkoutForm.get('addressForm').value)
            .subscribe(function (address) {
            _this.toastr.success('Address saved');
            _this.checkoutForm.get('addressForm').reset(address);
        }, function (error) {
            _this.toastr.error(error.message);
            console.log(error);
        });
    };
    __decorate([
        core_1.Input()
    ], CheckoutAddressComponent.prototype, "checkoutForm", void 0);
    CheckoutAddressComponent = __decorate([
        core_1.Component({
            selector: 'app-checkout-address',
            templateUrl: './checkout-address.component.html',
            styleUrls: ['./checkout-address.component.css']
        })
    ], CheckoutAddressComponent);
    return CheckoutAddressComponent;
}());
exports.CheckoutAddressComponent = CheckoutAddressComponent;
//# sourceMappingURL=checkout-address.component.js.map