"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutPaymentComponent = void 0;
var core_1 = require("@angular/core");
var stripe_js_1 = require("@stripe/stripe-js");
var CheckoutPaymentComponent = /** @class */ (function () {
    function CheckoutPaymentComponent(basketService, checkoutService, toastr, router) {
        this.basketService = basketService;
        this.checkoutService = checkoutService;
        this.toastr = toastr;
        this.router = router;
    }
    CheckoutPaymentComponent.prototype.ngAfterViewInit = function () {
        this.stripe = stripe_js_1.loadStripe('pk_test_51IlFRwImZ1r080cQ6cN3tqxlX2pQaR5NgY3OoLF6RuucZsF8uaaAikdRqajD2rjb8uBI80iDrpXTAI5u5JgQms8o00aHOISY2i');
        var elements = this.stripe.elements();
        this.cardNumber = elements.create('cardNumber');
        this.cardNumber.mount(this.cardNumberElement.nativeElement);
        this.cardExpiry = elements.create('cardExpiry');
        this.cardExpiry.mount(this.cardExpiryElement.nativeElement);
        this.cardCvc = elements.create('cardCvc');
        this.cardCvc.mount(this.cardCvcElement.nativeElement);
    };
    CheckoutPaymentComponent.prototype.ngOnDestroy = function () {
        this.cardNumber.destroy();
        this.cardExpiry.destroy();
        this.cardCvc.destroy();
    };
    CheckoutPaymentComponent.prototype.submitOrder = function () {
        var _this = this;
        var basket = this.basketService.getCurrentBasketValue();
        var orderToCreate = this.getOrderToCreate(basket);
        this.checkoutService.createOrder(orderToCreate).subscribe(function (order) {
            _this.toastr.success('Order created successfully');
            _this.basketService.deleteLocalBasket(basket.id);
            var navigationExtras = { state: order };
            _this.router.navigate(['checkout/success'], navigationExtras);
        }, function (error) {
            _this.toastr.error(error.message);
            console.log(error);
        });
    };
    CheckoutPaymentComponent.prototype.getOrderToCreate = function (basket) {
        return {
            basketId: basket.id,
            deliveryMethodId: +this.checkoutForm.get('deliveryForm').get('deliveryMethod').value,
            shipToAddress: this.checkoutForm.get('addressForm').value
        };
    };
    __decorate([
        core_1.Input()
    ], CheckoutPaymentComponent.prototype, "checkoutForm", void 0);
    __decorate([
        core_1.ViewChild('cardNumber', { static: true })
    ], CheckoutPaymentComponent.prototype, "cardNumberElement", void 0);
    __decorate([
        core_1.ViewChild('cardExpiry', { static: true })
    ], CheckoutPaymentComponent.prototype, "cardExpiryElement", void 0);
    __decorate([
        core_1.ViewChild('cardCvc', { static: true })
    ], CheckoutPaymentComponent.prototype, "cardCvcElement", void 0);
    CheckoutPaymentComponent = __decorate([
        core_1.Component({
            selector: 'app-checkout-payment',
            templateUrl: './checkout-payment.component.html',
            styleUrls: ['./checkout-payment.component.css']
        })
    ], CheckoutPaymentComponent);
    return CheckoutPaymentComponent;
}());
exports.CheckoutPaymentComponent = CheckoutPaymentComponent;
//# sourceMappingURL=checkout-payment.component.js.map