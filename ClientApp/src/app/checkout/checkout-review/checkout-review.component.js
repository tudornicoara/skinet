"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutReviewComponent = void 0;
var core_1 = require("@angular/core");
var CheckoutReviewComponent = /** @class */ (function () {
    function CheckoutReviewComponent(basketService, toastr) {
        this.basketService = basketService;
        this.toastr = toastr;
    }
    CheckoutReviewComponent.prototype.ngOnInit = function () {
        this.basket$ = this.basketService.basket$;
    };
    CheckoutReviewComponent.prototype.createPaymentIntent = function () {
        var _this = this;
        return this.basketService.createPaymentIntent().subscribe(function (response) {
            _this.toastr.success('Payment intent created');
            _this.appStepper.next();
        }, function (error) {
            console.log(error);
        });
    };
    __decorate([
        core_1.Input()
    ], CheckoutReviewComponent.prototype, "appStepper", void 0);
    CheckoutReviewComponent = __decorate([
        core_1.Component({
            selector: 'app-checkout-review',
            templateUrl: './checkout-review.component.html',
            styleUrls: ['./checkout-review.component.css']
        })
    ], CheckoutReviewComponent);
    return CheckoutReviewComponent;
}());
exports.CheckoutReviewComponent = CheckoutReviewComponent;
//# sourceMappingURL=checkout-review.component.js.map