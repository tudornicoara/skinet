"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutSuccessComponent = void 0;
var core_1 = require("@angular/core");
var CheckoutSuccessComponent = /** @class */ (function () {
    function CheckoutSuccessComponent(router) {
        this.router = router;
        var navigation = this.router.getCurrentNavigation();
        var state = navigation && navigation.extras && navigation.extras.state;
        if (state) {
            this.order = state;
        }
    }
    CheckoutSuccessComponent.prototype.ngOnInit = function () {
    };
    CheckoutSuccessComponent = __decorate([
        core_1.Component({
            selector: 'app-checkout-success',
            templateUrl: './checkout-success.component.html',
            styleUrls: ['./checkout-success.component.css']
        })
    ], CheckoutSuccessComponent);
    return CheckoutSuccessComponent;
}());
exports.CheckoutSuccessComponent = CheckoutSuccessComponent;
//# sourceMappingURL=checkout-success.component.js.map