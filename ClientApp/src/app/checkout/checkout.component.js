"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var CheckoutComponent = /** @class */ (function () {
    function CheckoutComponent(fb, accountService, basketService) {
        this.fb = fb;
        this.accountService = accountService;
        this.basketService = basketService;
    }
    CheckoutComponent.prototype.ngOnInit = function () {
        this.createCheckoutForm();
        this.getAddressFormValues();
        this.getDeliveryMethodValue();
        this.basketTotals$ = this.basketService.basketTotal$;
    };
    CheckoutComponent.prototype.createCheckoutForm = function () {
        this.checkoutForm = this.fb.group({
            addressForm: this.fb.group({
                firstName: [null, forms_1.Validators.required],
                lastName: [null, forms_1.Validators.required],
                street: [null, forms_1.Validators.required],
                city: [null, forms_1.Validators.required],
                state: [null, forms_1.Validators.required],
                zipcode: [null, forms_1.Validators.required]
            }),
            deliveryForm: this.fb.group({
                deliveryMethod: [null, forms_1.Validators.required]
            }),
            paymentForm: this.fb.group({
                nameOnCard: [null, forms_1.Validators.required]
            })
        });
    };
    CheckoutComponent.prototype.getAddressFormValues = function () {
        var _this = this;
        this.accountService.getUserAddress().subscribe(function (address) {
            if (address) {
                _this.checkoutForm.get('addressForm').patchValue(address);
            }
        }, function (error) {
            console.log(error);
        });
    };
    CheckoutComponent.prototype.getDeliveryMethodValue = function () {
        var basket = this.basketService.getCurrentBasketValue();
        if (basket.deliveryMethodId != null) {
            this.checkoutForm.get('deliveryForm').get('deliveryMethod')
                .patchValue(basket.deliveryMethodId.toString());
        }
    };
    CheckoutComponent = __decorate([
        core_1.Component({
            selector: 'app-checkout',
            templateUrl: './checkout.component.html',
            styleUrls: ['./checkout.component.css']
        })
    ], CheckoutComponent);
    return CheckoutComponent;
}());
exports.CheckoutComponent = CheckoutComponent;
//# sourceMappingURL=checkout.component.js.map