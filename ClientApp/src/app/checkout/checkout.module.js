"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var checkout_component_1 = require("./checkout.component");
var checkout_routing_module_1 = require("./checkout-routing.module");
var shared_module_1 = require("../shared/shared.module");
var checkout_address_component_1 = require("./checkout-address/checkout-address.component");
var checkout_delivery_component_1 = require("./checkout-delivery/checkout-delivery.component");
var checkout_review_component_1 = require("./checkout-review/checkout-review.component");
var checkout_payment_component_1 = require("./checkout-payment/checkout-payment.component");
var checkout_success_component_1 = require("./checkout-success/checkout-success.component");
var CheckoutModule = /** @class */ (function () {
    function CheckoutModule() {
    }
    CheckoutModule = __decorate([
        core_1.NgModule({
            declarations: [checkout_component_1.CheckoutComponent, checkout_address_component_1.CheckoutAddressComponent, checkout_delivery_component_1.CheckoutDeliveryComponent, checkout_review_component_1.CheckoutReviewComponent, checkout_payment_component_1.CheckoutPaymentComponent, checkout_success_component_1.CheckoutSuccessComponent],
            imports: [
                common_1.CommonModule,
                checkout_routing_module_1.CheckoutRoutingModule,
                shared_module_1.SharedModule
            ]
        })
    ], CheckoutModule);
    return CheckoutModule;
}());
exports.CheckoutModule = CheckoutModule;
//# sourceMappingURL=checkout.module.js.map