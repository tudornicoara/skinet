"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CheckoutService = void 0;
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var environment_1 = require("../../environments/environment");
var CheckoutService = /** @class */ (function () {
    function CheckoutService(http) {
        this.http = http;
        this.baseUrl = environment_1.environment.apiUrl;
    }
    CheckoutService.prototype.createOrder = function (order) {
        return this.http.post(this.baseUrl + 'orders', order);
    };
    CheckoutService.prototype.getDeliveryMethods = function () {
        return this.http.get(this.baseUrl + 'orders/deliveryMethods').pipe(operators_1.map(function (dm) {
            return dm.sort(function (a, b) { return b.price - a.price; });
        }));
    };
    CheckoutService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], CheckoutService);
    return CheckoutService;
}());
exports.CheckoutService = CheckoutService;
//# sourceMappingURL=checkout.service.js.map