import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { BasketService } from "../../basket/basket.service";
import { IBasket } from "../../shared/models/basket";
import { IUser } from "../../shared/models/user";
import { AccountService } from "../../account/account.service";
import { ToastrService } from "ngx-toastr";

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})

export class NavBarComponent implements OnInit{
  basket$: Observable<IBasket>;
  currentUser$: Observable<IUser>;

  constructor(private basketService: BasketService,
    private accountService: AccountService,
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.basket$ = this.basketService.basket$;
    this.currentUser$ = this.accountService.currentUser$;
  }

  logout() {
    this.accountService.logout();
    this.toastr.success('Logged out');
  }
}
