"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasketSummaryComponent = void 0;
var core_1 = require("@angular/core");
var BasketSummaryComponent = /** @class */ (function () {
    function BasketSummaryComponent(basketService) {
        this.basketService = basketService;
        this.decrement = new core_1.EventEmitter();
        this.increment = new core_1.EventEmitter();
        this.remove = new core_1.EventEmitter();
        this.isBasket = true;
    }
    BasketSummaryComponent.prototype.ngOnInit = function () {
        this.basket$ = this.basketService.basket$;
    };
    BasketSummaryComponent.prototype.decrementItemQuantity = function (item) {
        this.decrement.emit(item);
    };
    BasketSummaryComponent.prototype.incrementItemQuantity = function (item) {
        this.increment.emit(item);
    };
    BasketSummaryComponent.prototype.removeBasketItem = function (item) {
        this.remove.emit(item);
    };
    __decorate([
        core_1.Output()
    ], BasketSummaryComponent.prototype, "decrement", void 0);
    __decorate([
        core_1.Output()
    ], BasketSummaryComponent.prototype, "increment", void 0);
    __decorate([
        core_1.Output()
    ], BasketSummaryComponent.prototype, "remove", void 0);
    __decorate([
        core_1.Input()
    ], BasketSummaryComponent.prototype, "isBasket", void 0);
    BasketSummaryComponent = __decorate([
        core_1.Component({
            selector: 'app-basket-summary',
            templateUrl: './basket-summary.component.html',
            styleUrls: ['./basket-summary.component.css']
        })
    ], BasketSummaryComponent);
    return BasketSummaryComponent;
}());
exports.BasketSummaryComponent = BasketSummaryComponent;
//# sourceMappingURL=basket-summary.component.js.map