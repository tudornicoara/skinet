"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderTotalsComponent = void 0;
var core_1 = require("@angular/core");
var OrderTotalsComponent = /** @class */ (function () {
    function OrderTotalsComponent(basketService) {
        this.basketService = basketService;
    }
    OrderTotalsComponent.prototype.ngOnInit = function () {
        this.basketTotal$ = this.basketService.basketTotal$;
    };
    OrderTotalsComponent = __decorate([
        core_1.Component({
            selector: 'app-order-totals',
            templateUrl: './order-totals.component.html',
            styleUrls: ['./order-totals.component.css']
        })
    ], OrderTotalsComponent);
    return OrderTotalsComponent;
}());
exports.OrderTotalsComponent = OrderTotalsComponent;
//# sourceMappingURL=order-totals.component.js.map