"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagerComponent = void 0;
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var PagerComponent = /** @class */ (function () {
    function PagerComponent() {
        this.pageChanged = new core_1.EventEmitter();
    }
    PagerComponent.prototype.ngOnInit = function () {
    };
    PagerComponent.prototype.onPagerChange = function (event) {
        this.pageChanged.emit(event.page);
    };
    __decorate([
        core_2.Input()
    ], PagerComponent.prototype, "totalCount", void 0);
    __decorate([
        core_2.Input()
    ], PagerComponent.prototype, "pageSize", void 0);
    __decorate([
        core_2.Output()
    ], PagerComponent.prototype, "pageChanged", void 0);
    PagerComponent = __decorate([
        core_2.Component({
            selector: 'app-pager',
            templateUrl: './pager.component.html',
            styleUrls: ['./pager.component.css']
        })
    ], PagerComponent);
    return PagerComponent;
}());
exports.PagerComponent = PagerComponent;
//# sourceMappingURL=pager.component.js.map