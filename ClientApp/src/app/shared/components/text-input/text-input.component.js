"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextInputComponent = void 0;
var core_1 = require("@angular/core");
var TextInputComponent = /** @class */ (function () {
    function TextInputComponent(controlDir) {
        this.controlDir = controlDir;
        this.type = 'text';
        this.controlDir.valueAccessor = this;
    }
    TextInputComponent.prototype.ngOnInit = function () {
        var control = this.controlDir.control;
        var validators = control.validator ? [control.validator] : [];
        var asyncValidators = control.asyncValidator ? [control.asyncValidator] : [];
        control.setValidators(validators);
        control.setAsyncValidators(asyncValidators);
        control.updateValueAndValidity();
    };
    TextInputComponent.prototype.onChange = function (event) { };
    TextInputComponent.prototype.onTouched = function () { };
    TextInputComponent.prototype.writeValue = function (obj) {
        this.input.nativeElement.value = obj || '';
    };
    TextInputComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    TextInputComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    __decorate([
        core_1.ViewChild('input', { static: true })
    ], TextInputComponent.prototype, "input", void 0);
    __decorate([
        core_1.Input()
    ], TextInputComponent.prototype, "type", void 0);
    __decorate([
        core_1.Input()
    ], TextInputComponent.prototype, "label", void 0);
    TextInputComponent = __decorate([
        core_1.Component({
            selector: 'app-text-input',
            templateUrl: './text-input.component.html',
            styleUrls: ['./text-input.component.css']
        }),
        __param(0, core_1.Self())
    ], TextInputComponent);
    return TextInputComponent;
}());
exports.TextInputComponent = TextInputComponent;
//# sourceMappingURL=text-input.component.js.map