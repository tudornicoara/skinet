"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SharedModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var pagination_1 = require("ngx-bootstrap/pagination");
var paging_header_component_1 = require("./components/paging-header/paging-header.component");
var pager_component_1 = require("./components/pager/pager.component");
var carousel_1 = require("ngx-bootstrap/carousel");
var order_totals_component_1 = require("./components/order-totals/order-totals.component");
var forms_1 = require("@angular/forms");
var dropdown_1 = require("ngx-bootstrap/dropdown");
var text_input_component_1 = require("./components/text-input/text-input.component");
var stepper_1 = require("@angular/cdk/stepper");
var stepper_component_1 = require("./components/stepper/stepper.component");
var basket_summary_component_1 = require("./components/basket-summary/basket-summary.component");
var router_1 = require("@angular/router");
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        core_1.NgModule({
            declarations: [
                paging_header_component_1.PagingHeaderComponent,
                pager_component_1.PagerComponent,
                order_totals_component_1.OrderTotalsComponent,
                text_input_component_1.TextInputComponent,
                stepper_component_1.StepperComponent,
                basket_summary_component_1.BasketSummaryComponent
            ],
            imports: [
                common_1.CommonModule,
                pagination_1.PaginationModule.forRoot(),
                carousel_1.CarouselModule.forRoot(),
                dropdown_1.BsDropdownModule.forRoot(),
                forms_1.ReactiveFormsModule,
                stepper_1.CdkStepperModule,
                router_1.RouterModule
            ],
            exports: [
                pagination_1.PaginationModule,
                paging_header_component_1.PagingHeaderComponent,
                pager_component_1.PagerComponent,
                carousel_1.CarouselModule,
                order_totals_component_1.OrderTotalsComponent,
                forms_1.ReactiveFormsModule,
                dropdown_1.BsDropdownModule,
                text_input_component_1.TextInputComponent,
                stepper_1.CdkStepperModule,
                stepper_component_1.StepperComponent,
                basket_summary_component_1.BasketSummaryComponent
            ]
        })
    ], SharedModule);
    return SharedModule;
}());
exports.SharedModule = SharedModule;
//# sourceMappingURL=shared.module.js.map