﻿namespace Skinet.Entities
{
    public class ProductBrand : BaseEntity
    {
        public string Name { get; set; }
    }
}
