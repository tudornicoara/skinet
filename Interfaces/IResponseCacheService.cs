﻿using System;
using System.Threading.Tasks;

namespace Skinet.Interfaces
{
    internal interface IResponseCacheService
    {
        Task CacheResponseAsync(string cacheKey, object response, TimeSpan timeToLive);
        Task<string> GetChachedResponseAsync(string cacheKey);
    }
}
