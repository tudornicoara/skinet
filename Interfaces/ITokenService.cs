﻿using Skinet.Entities.Identity;

namespace Skinet.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(AppUser user);
    }
}
