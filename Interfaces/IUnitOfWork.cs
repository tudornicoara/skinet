﻿using System;
using System.Threading.Tasks;
using Skinet.Entities;

namespace Skinet.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<TEntity> Repository<TEntity>() where TEntity : BaseEntity;

        Task<int> Complete();
    }
}
